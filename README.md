# ArtGram

ArtGram is an Android Mobile Application that enables us to See, Search and Save [Art Institute Chicago](https://www.artic.edu/) Collection as Our Favorite Items. Before visit the museum offline, it's better for us as a visitor to check and remember which collection that we want to see, in the museum.

## Assumptions:

1. Users Mobile Phone with Android < 5.0 will not be able to install it.
2. The Application will run on approximately 98% of Android Device.
3. The Project not tested to run and build on IOS even though using react native as framework.
4. The Product developed within 5 days.
5. The Search Function will return blurred base64 image due to the response of API. It will be a problem for users.

## Dependencies:

1. Build using React Native v0.67.4
2. Using Android API 21: Android 5.0 (Lollipop) as minimum SDK Version.
3. Using API from [Art Institute Chicago](http://api.artic.edu/docs/)
4. Using third party libraries:
   - [axios](https://www.npmjs.com/package/axios)
   - [react-navigation](https://reactnavigation.org/)
   - [react-native-config](https://www.npmjs.com/package/react-native-config)
   - [react-native-flash-message](https://www.npmjs.com/package/react-native-flash-message)
   - [react-native-gesture-handler](https://www.npmjs.com/package/react-native-gesture-handler)
   - [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)

## Instruction

### Preparation

Clone this repository and run to install all dependencies

```bash
npm install
```

Create .env file which contain base url for the API at root of project

```javascript
BASE_URL = 'https://api.artic.edu/api/v1/artworks';
```

#

### Run on Emulator or Physical Device

- Make sure device detected

```bash
adb devices
```

Run on Device

```bash
npm run android
```

#

### Perform Testing

```bash
npm run test
```

#

#

### Build APK File

- Make sure you already generate _.keystore file and put it in _{rootDirectory}/android/app/\*
- Add following lines to your _{rootDirectory}/android/gradle.properties_ file based on keystore file you have generated

```javascript
MYAPP_UPLOAD_STORE_FILE={your-keystore-filename}.keystore
MYAPP_UPLOAD_KEY_ALIAS={your-key-alias}
MYAPP_UPLOAD_STORE_PASSWORD={your-store-password}
MYAPP_UPLOAD_KEY_PASSWORD={your-key-password}
```

- Add following lines to your _{rootDirectory}/android/app/build.gradle_ file based on keystore file you have generated

```javascript
android {
    ///
    signingConfigs {
        release {
            if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
                storeFile file(MYAPP_UPLOAD_STORE_FILE)
                storePassword MYAPP_UPLOAD_STORE_PASSWORD
                keyAlias MYAPP_UPLOAD_KEY_ALIAS
                keyPassword MYAPP_UPLOAD_KEY_PASSWORD
            }
        }
        ///
    }
    ///
}
```

- and modify this line

```javascript
android {
    ///
    signingConfigs {
        ///
        buildTypes {
            ///
            release {
                ///
                signingConfig signingConfigs.release //modify from signingConfig signingConfigs.debug
                ///
            }
        }

    }
    ...
}
```

- run console on project root directory

```bash
cd android
./gradlew assembleRelease
```

- if the build success, the APK file can be found on _{rootDirectory}/android/app/build/outputs/apk/release_

## User Guide

- Open the Application, after Splash Screen (Art Institute Chicago Logo), you will redirected to Home Screen.
- Home Screen contain 3 main section
  - Search Bar: type a keyword and press enter to search related collection
  - Thumbnail Section: contain thumbnails of the collection, scroll it to explore. Collections thumbnail based on searched keyword will also appeared in this section.
  - Bottom Tab Navigation Bar : Used for navigating between Home and Favorite Screen
- Favorite Screen: Contain thumbnail, title, artist, and type of collection that you have save before. You can delete collection from your favorite page by pressing delete button.
- Detail Screen: Contain detail information of a collection, you can access the page by pressing thumbnail on Home Screen or Favorite Card Item. You can add or remove the collection on detail page by pressing heart button.

## Future Enhancement

- Using Authentication API to store credential
  - Credential can be use for storing user data include favorite collections. For latest release, favorite collections stored on Mobile Phone Local Storage. So we can't check our favorite item from the other Mobile Phone.
  - Can be use as Social Media App, so we can share, comment, and doing any social media apps behavior.

## Update v1.0.2

- Add Exception Handling, and show message if failed to get response from API.
- Change Url Params on API Request so we can get only needed properties from API
- Implements redux to improve user experience.
- Implements hit API on Splash screen and store to redux.
- Fix 'Show More' button on Detail Card, only showed when number of lines > 5.
- Delete API getImageDetail, using passing props to Detail Page instead.

## Download Link

[GDrive](https://drive.google.com/drive/folders/1p0z-5dPBvx-w9_R9vqaeE-TWH4050Cif?usp=sharing)
