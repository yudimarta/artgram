jest.useFakeTimers();

import React from 'react';
import renderer from 'react-test-renderer';
import {
  BlankFavoritePage,
  DeleteModal,
  DetailCard,
  DetailModal,
  DetailThumbnail,
  FavoriteCard,
  Header,
  Loading,
  PaintingThumbnail,
  SearchBar,
  TabItem,
} from '../src/presentation/components';

const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn(),
  },
  ...props,
});

describe('renders Components correctly', () => {
  let props: any;
  beforeEach(() => {
    props = createTestProps({});
  });
  test('renders Detail Thumbnail correctly', () => {
    const detailthumbnail = renderer
      .create(
        <DetailThumbnail imageId={'2d484387-2509-5e8e-2c43-22f9981972eb'} />,
      )
      .toJSON();
    expect(detailthumbnail).toMatchSnapshot();
  });

  test('renders Painting Thumbnail correctly', () => {
    const paintingthumbnail = renderer
      .create(
        <PaintingThumbnail
          imageUrl={'2d484387-2509-5e8e-2c43-22f9981972eb'}
          IsSearchedImage={false}
          onPress={function (): void {
            props.navigation.navigate('Detail', 27992);
          }}
        />,
      )
      .toJSON();
    expect(paintingthumbnail).toMatchSnapshot();
  });

  test('renders TabItem correctly', () => {
    const tabitem = renderer
      .create(
        <TabItem
          title={'Home'}
          isActive={false}
          onPress={function (): void {
            props.navigation.emit({
              type: 'tabPress',
              canPreventDefault: true,
            });
          }}
          onLongPress={function (): void {
            props.navigation.emit({
              type: 'tabLongPress',
            });
          }}
        />,
      )
      .toJSON();
    expect(tabitem).toMatchSnapshot();
  });

  test('renders Blank Favorite Page correctly', () => {
    const blankfavoritepage = renderer
      .create(
        <BlankFavoritePage
          onPress={function (): void {
            props.navigation.replace('MainApp');
          }}
        />,
      )
      .toJSON();
    expect(blankfavoritepage).toMatchSnapshot();
  });

  test('renders Delete Modal correctly', () => {
    const deletemodal = renderer
      .create(
        <DeleteModal
          isVisible={true}
          onRequestClose={function (): void {}}
          onPress={function (): void {}}
          onDelete={function (): void {}}
        />,
      )
      .toJSON();
    expect(deletemodal).toMatchSnapshot();
  });

  test('renders Detail Card correctly', () => {
    const detailcard = renderer
      .create(
        <DetailCard
          title={'Publication History'}
          content={'history'}
          onPress={function (): void {}}
        />,
      )
      .toJSON();
    expect(detailcard).toMatchSnapshot();
  });

  test('renders Detail Modal correctly', () => {
    const detailmodal = renderer
      .create(
        <DetailModal
          title={'Publication History'}
          content={'history'}
          isVisible={false}
          onRequestClose={function (): void {}}
          onPress={function (): void {}}
        />,
      )
      .toJSON();
    expect(detailmodal).toMatchSnapshot();
  });

  test('renders Favorite Card correctly', () => {
    const favoritecard = renderer
      .create(
        <FavoriteCard
          title={'A Sunday on La Grande Jatte — 1884'}
          imageUrl={'2d484387-2509-5e8e-2c43-22f9981972eb'}
          artist={'Georges Seurat\nFrench, 1859-1891'}
          category={'Painting'}
          onPress={function (): void {}}
          onDelete={function (): void {}}
        />,
      )
      .toJSON();
    expect(favoritecard).toMatchSnapshot();
  });

  test('renders Header correctly', () => {
    const header = renderer
      .create(<Header onPress={function (): void {}} />)
      .toJSON();
    expect(header).toMatchSnapshot();
  });

  test('renders Loading correctly', () => {
    const loading = renderer.create(<Loading />).toJSON();
    expect(loading).toMatchSnapshot();
  });

  test('renders SearchBar correctly', () => {
    const searchbar = renderer
      .create(
        <SearchBar
          searchedKeyWord={'Monet'}
          placeHolder={'Search Your Taste'}
          onPress={function (): void {}}
          onSubmitEditing={function (): void {}}
        />,
      )
      .toJSON();
    expect(searchbar).toMatchSnapshot();
  });
});
