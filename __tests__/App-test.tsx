jest.useFakeTimers();

import React from 'react';
import renderer from 'react-test-renderer';
import { Splash, Home, Detail, Favorite } from '../src/presentation/pages';

jest.mock('@react-navigation/native', () => ({
  useIsFocused: jest.fn(),
}));

describe('renders Screens/Pages correctly', () => {
  test('renders Splash Screen correctly', () => {
    const splash = renderer.create(<Splash navigation />).toJSON();
    expect(splash).toMatchSnapshot();
  });

  test('renders Home Screen correctly', () => {
    const tree = renderer.create(<Home navigation />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('renders Detail Screen correctly', () => {
    const detail = renderer
      .create(<Detail navigation route={27992} />)
      .toJSON();
    expect(detail).toMatchSnapshot();
  });

  test('renders Favorite Screen correctly', () => {
    const favorite = renderer.create(<Favorite navigation />).toJSON();
    expect(favorite).toMatchSnapshot();
  });
});
