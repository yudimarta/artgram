import AsyncStorage from '@react-native-async-storage/async-storage';
import { showError } from '../utilities';

export const storeDataFavorite = async (value: any) => {
  try {
    const jsonData = JSON.stringify(value);
    let existingItems = await getFavoriteItems();
    const updatedItems = [...existingItems, jsonData];
    await AsyncStorage.setItem('@favorite', JSON.stringify(updatedItems));
  } catch (err) {
    showError('Error Add Favorite Items');
  }
};

export const storeDataAfterRemoved = async (value: any) => {
  try {
    await AsyncStorage.setItem('@favorite', JSON.stringify(value));
  } catch (err) {
    showError('Error Add Favorite Items');
  }
};

const getFavoriteItems = async () => {
  let items = await AsyncStorage.getItem('@favorite');
  if (items) {
    return JSON.parse(items);
  } else {
    return [];
  }
};

export const getDataFavorite = async () => {
  try {
    const value = await AsyncStorage.getItem('@favorite');
    if (value !== null) {
      return JSON.parse(value);
    } else {
      return null;
    }
  } catch (e) {
    showError('Error Getting Favorite Items');
  }
};

export const removeDataFavorite = async () => {
  try {
    await AsyncStorage.removeItem('@favorite');
  } catch (e) {
    showError('Error Delete Favorite Item');
  }
};
