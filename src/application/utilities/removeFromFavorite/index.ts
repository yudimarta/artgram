export const removeFromFavorite = (favorite: any[], detail: any) => {
  for (let i = 0; i < favorite.length; i++) {
    if (JSON.parse(favorite[i]).id === detail.id) {
      favorite.splice(i, 1);
    } else {
      continue;
    }
  }
  return favorite;
};
