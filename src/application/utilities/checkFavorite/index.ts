export function checkFavorite(favorite: any[], detail: any) {
  let checkResult = false;
  for (let i = 0; i < favorite.length; i++) {
    if (JSON.parse(favorite[i]).id === detail.id) {
      checkResult = true;
    } else {
      continue;
    }
  }
  return checkResult;
}
