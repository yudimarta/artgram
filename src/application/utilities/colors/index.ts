const mainColors = {
  red1: '#A21313',
  green1: '#104E5B',
  green2: '#0BCAD4',
  orange1: '#FDC27A',
  white: '#FFFFFF',
  black: '#000000',
  black2: 'rgba(0, 0, 0, 0.5)',
  grey1: '#616F80',
  grey2: '#104E5B',
  grey3: '#E5E5E5',
  grey4: '#EDEEF0',
  grey5: '#495A75',
  grey6: '#F8F8F8',
  blue1: '#0066CB',
};

export const colors = {
  primary: mainColors.green1,
  secondary: mainColors.orange1,
  tsel: mainColors.red1,
  text: {
    primary: mainColors.black,
    secondary: mainColors.grey1,
    tertiary: mainColors.grey3,
    menuActive: mainColors.orange1,
    menuInactive: mainColors.grey1,
    link: mainColors.blue1,
  },
  button: {
    primary: {
      background: mainColors.orange1,
      text: mainColors.black,
    },
    disable: {
      background: mainColors.grey1,
      text: mainColors.grey4,
    },
  },
  loadingBackground: mainColors.black2,
  paintingBackground: mainColors.grey6,
  error: mainColors.red1,
  success: mainColors.green2,
  black: mainColors.black,
  white: mainColors.white,
};
