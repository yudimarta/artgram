export const LIMIT_PER_PAGE = 15;
export const ITEMS =
  'image_id,id,artist_title,inscriptions,provenance_text.publication_history,exhibition_history,artwork_type_title,title';
export const NUM_OF_LINES = 5;
