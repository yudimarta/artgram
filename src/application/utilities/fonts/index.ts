export const fonts = {
  telkomsel: {
    400: 'TelkomselBatikSans-Regular',
    700: 'TelkomselBatikSans-Bold',
  },
  primary: {
    200: 'Poppins-ExtraLight',
    300: 'Poppins-Light',
    400: 'Poppins-Regular',
    600: 'Poppins-SemiBold',
    700: 'Poppins-Bold',
    800: 'Poppins-ExtraBold',
    900: 'Poppins-Black',
    normal: 'Poppins-Regular',
  },
};
