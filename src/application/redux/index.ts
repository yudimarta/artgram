import { createStore } from 'redux';

const initialState = {
  loading: false,
  items: [],
  items_searched: [],
  page_items: 1,
  page_search: 1,
  search_keyword: '',
};

const reducer = (state = initialState, action: any) => {
  if (action.type === 'SET_LOADING') {
    return {
      ...state,
      loading: action.value,
    };
  }
  if (action.type === 'SET_ITEMS') {
    return {
      ...state,
      items: action.value,
    };
  }
  if (action.type === 'SET_SEARCH') {
    return {
      ...state,
      items_searched: action.value,
    };
  }
  if (action.type === 'SET_PAGE_ITEMS') {
    return {
      ...state,
      page_items: action.value,
    };
  }
  if (action.type === 'SET_PAGE_SEARCH') {
    return {
      ...state,
      page_search: action.value,
    };
  }
  if (action.type === 'SET_SEARCHED_KEYWORD') {
    return {
      ...state,
      search_keyword: action.value,
    };
  }
  return state;
};

const store = createStore(reducer);

export default store;
