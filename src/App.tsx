import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './presentation/routes';
import { LogBox } from 'react-native';
import FlashMessage from 'react-native-flash-message';
import 'react-native-gesture-handler';
import { Provider, useSelector, RootStateOrAny } from 'react-redux';
import store from './application/redux';
import { Loading } from './presentation/components';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);
LogBox.ignoreLogs(['Remote debugger']);

const MainApp = () => {
  const stateGlobal = useSelector((state: RootStateOrAny) => state);
  return (
    <>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
      {stateGlobal.loading && <Loading />}
    </>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
};

export default App;
