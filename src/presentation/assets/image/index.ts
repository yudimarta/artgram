import articLogo from './articlogo.png';
import emptyFolder from './empty-folder.png';
import Folder from './folder.png';

export { articLogo, emptyFolder, Folder };
