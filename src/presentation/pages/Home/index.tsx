import { View, FlatList } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import { showError, showSuccess } from '../../../application/';
import { BlankHomePage, PaintingThumbnail, SearchBar } from '../../components';
import {
  ImagePagination,
  searchImage,
  getImageByPaginate,
} from '../../../infrastructure/';
import { styles } from './home-styles';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';

const Home = ({ navigation }: { navigation: any }) => {
  const [resfesh, setRefresh] = useState(false);
  const dispatch = useDispatch();
  const stateGlobal = useSelector((state: RootStateOrAny) => state);

  const getImageByPaginateList = useCallback(
    async (pages: number) => {
      const data = await getImageByPaginate(pages);
      if (data) {
        dispatch({
          type: 'SET_ITEMS',
          value: data,
        });
        setRefresh(false);
      } else {
        setRefresh(false);
        showError(
          'Sorry, there is a problem fetching the data, please try again later',
        );
      }
      dispatch({
        type: 'SET_LOADING',
        value: false,
      });
    },
    [dispatch],
  );

  const getSearchedImage = useCallback(
    async (pages: number, keyword: string) => {
      dispatch({
        type: 'SET_LOADING',
        value: true,
      });
      const data = await searchImage(pages, keyword);
      if (data) {
        dispatch({
          type: 'SET_SEARCH',
          value: data,
        });
        setRefresh(false);
      } else {
        setRefresh(false);
        showError(
          'Sorry, there is a problem fetching the data, please try again later',
        );
      }
      dispatch({
        type: 'SET_LOADING',
        value: false,
      });
    },
    [dispatch],
  );

  useEffect(() => {
    if (stateGlobal.search_keyword === '') {
      stateGlobal.items ? null : getImageByPaginateList(1);
    }
  }, [
    getImageByPaginateList,
    getSearchedImage,
    stateGlobal.items,
    stateGlobal.search_keyword,
  ]);

  const handleRefresh = () => {
    setRefresh(true);
    dispatch({
      type: 'SET_PAGE_ITEMS',
      value: 1,
    });
    getImageByPaginateList(1);
  };

  const handleRefreshSearch = () => {
    setRefresh(true);
    dispatch({
      type: 'SET_PAGE_ITEMS',
      value: 1,
    });
    getSearchedImage(1, stateGlobal.search_keyword);
  };

  const handleNextPage = async (pages: number) => {
    const nextDataImage = await getImageByPaginate(pages);
    if (nextDataImage !== null) {
      if (nextDataImage.length === 0) {
        showSuccess('No More to Reload');
        setRefresh(false);
        dispatch({
          type: 'SET_PAGE_ITEMS',
          value: 1,
        });
      } else {
        let NewData = stateGlobal.items.concat(nextDataImage);
        dispatch({
          type: 'SET_ITEMS',
          value: NewData,
        });
        setRefresh(false);
      }
    } else {
      setRefresh(false);
      showError(
        'Sorry, there is a problem fetching the data, please try again later',
      );
    }
  };

  const handleNextPageSearch = async (pages: number, keyword: string) => {
    const nextDataImage = await searchImage(pages, keyword);
    if (nextDataImage !== null) {
      if (nextDataImage.length === 0) {
        showSuccess('No More to Reload');
        setRefresh(false);
        dispatch({
          type: 'SET_PAGE_SEARCH',
          value: 1,
        });
      } else {
        let NewData = stateGlobal.items_searched.concat(nextDataImage);
        dispatch({
          type: 'SET_SEARCH',
          value: NewData,
        });
        setRefresh(false);
      }
    } else {
      setRefresh(false);
      showError(
        'Sorry, there is a problem fetching the data, please try again later',
      );
    }
  };

  const handleLoadNextPage = () => {
    dispatch({
      type: 'SET_PAGE_ITEMS',
      value: stateGlobal.page_items + 1,
    });
    setRefresh(true);
    handleNextPage(stateGlobal.page_items + 1);
  };

  const handleLoadNextSearch = () => {
    dispatch({
      type: 'SET_PAGE_SEARCH',
      value: stateGlobal.page_search + 1,
    });
    setRefresh(true);
    handleNextPageSearch(
      stateGlobal.page_search + 1,
      stateGlobal.search_keyword,
    );
  };

  const renderItems = ({ item }: { item: ImagePagination }) => {
    return (
      <PaintingThumbnail
        imageUrl={item.image_id}
        onPress={() => navigation.navigate('Detail', item)}
      />
    );
  };

  const renderItemsSearch = ({ item }: { item: ImagePagination }) => {
    return (
      <PaintingThumbnail
        imageUrl={item.image_id}
        onPress={() => navigation.navigate('Detail', item)}
      />
    );
  };

  return (
    <>
      <View style={styles.page}>
        <View style={styles.content}>
          <SearchBar
            searchedKeyWord={stateGlobal.search_keyword}
            placeHolder="Search Your Taste"
            onPress={() => {
              dispatch({
                type: 'SET_SEARCHED_KEYWORD',
                value: '',
              });
              dispatch({
                type: 'SET_SEARCH',
                value: [],
              });
            }}
            onChangeText={(val: string) => {
              dispatch({
                type: 'SET_SEARCHED_KEYWORD',
                value: val,
              });
              if (val === '') {
                dispatch({
                  type: 'SET_SEARCHED_KEYWORD',
                  value: '',
                });
                dispatch({
                  type: 'SET_SEARCH',
                  value: [],
                });
              }
            }}
            onSubmitEditing={() => {
              getSearchedImage(
                stateGlobal.page_search,
                stateGlobal.search_keyword,
              );
            }}
          />
          {stateGlobal.items.length === 0 && stateGlobal.loading === false ? (
            <BlankHomePage onPress={handleRefresh} />
          ) : (
            <FlatList
              data={
                stateGlobal.items_searched.length > 0
                  ? stateGlobal.items_searched
                  : stateGlobal.items
              }
              renderItem={
                stateGlobal.items_searched.length > 0
                  ? renderItemsSearch
                  : renderItems
              }
              keyExtractor={item => item.id.toString()}
              numColumns={3}
              columnWrapperStyle={styles.flatListContainer}
              refreshing={resfesh}
              onRefresh={
                stateGlobal.items_searched.length > 0
                  ? handleRefreshSearch
                  : handleRefresh
              }
              onEndReached={
                stateGlobal.items_searched.length > 0
                  ? handleLoadNextSearch
                  : handleLoadNextPage
              }
            />
          )}
        </View>
      </View>
    </>
  );
};

export default Home;
