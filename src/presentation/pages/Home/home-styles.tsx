import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../application';

export const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 4,
  },
  textResult: {
    fontSize: 14,
    paddingHorizontal: 24,
    marginTop: 4,
    fontFamily: fonts.primary[300],
    marginBottom: 8,
  },
  imageHeight: {
    maxHeight: 120,
    maxWidth: 120,
    borderRadius: 30,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  flatListContainer: {
    flex: 1,
    marginTop: 12,
    justifyContent: 'space-around',
    paddingHorizontal: 24,
  },
});
