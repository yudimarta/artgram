import Home from './Home';
import Splash from './Splash';
import Favorite from './Favorite';
import Detail from './Detail';

export { Home, Splash, Favorite, Detail };
