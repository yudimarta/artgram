import { View, ScrollView } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import { BlankFavoritePage, DeleteModal, FavoriteCard } from '../../components';
import { useIsFocused } from '@react-navigation/native';
import {
  removeDataFavorite,
  removeFromFavorite,
  showError,
  showSuccess,
  storeDataAfterRemoved,
  getDataFavorite,
} from '../../../application';
import { styles } from './favorite-styles';

const Favorite = ({ navigation }: { navigation: any }) => {
  const isFocused = useIsFocused();
  const [favoriteList, setFavoriteList] = useState<any[]>([]);
  const [modalDeleteVisible, setModalDeleteVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});

  const getDataFavoriteFromLocal = useCallback(async () => {
    const result = await getDataFavorite();
    if (result) {
      setFavoriteList(result);
    }
  }, []);

  const removeFavoriteItem = async (item: any) => {
    try {
      const result = await getDataFavorite();
      if (result) {
        const afterRemoved = removeFromFavorite(result, item);
        await removeDataFavorite();
        storeDataAfterRemoved(afterRemoved);
        setModalDeleteVisible(false);
        getDataFavoriteFromLocal();
        showSuccess('Removed from Favorite');
      }
    } catch (e) {
      showError('Error remove Item from Favorite');
    }
  };

  useEffect(() => {
    getDataFavoriteFromLocal();
    return () => {
      console.log('-- Favorite components cleanup and unmounted');
    };
  }, [getDataFavoriteFromLocal, isFocused]);

  return (
    <>
      <View style={styles.page}>
        {favoriteList.length > 0 ? (
          <ScrollView
            style={styles.content}
            showsVerticalScrollIndicator={false}>
            {favoriteList.map((item, index) => {
              item = JSON.parse(item);
              return (
                <FavoriteCard
                  key={index}
                  title={item.title}
                  imageUrl={item.image_id}
                  artist={item.artist_title}
                  category={item.artwork_type_title}
                  onPress={() => navigation.navigate('Detail', item)}
                  onDelete={() => {
                    setModalDeleteVisible(true);
                    setSelectedItem(item);
                  }}
                />
              );
            })}
          </ScrollView>
        ) : (
          <View style={styles.content}>
            <BlankFavoritePage onPress={() => navigation.replace('MainApp')} />
          </View>
        )}
      </View>
      <DeleteModal
        isVisible={modalDeleteVisible}
        onRequestClose={() => setModalDeleteVisible(!modalDeleteVisible)}
        onPress={() => setModalDeleteVisible(!modalDeleteVisible)}
        onDelete={() => removeFavoriteItem(selectedItem)}
      />
    </>
  );
};

export default Favorite;
