import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts } from '../../../application';

export const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.paintingBackground,
  },
  label: {
    fontSize: 14,
    fontFamily: fonts.primary[700],
    color: colors.text.secondary,
    marginLeft: 16,
  },
  ImageContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  ImageSize: {
    height: Dimensions.get('window').width * 0.6,
    width: Dimensions.get('window').width * 0.6,
    borderRadius: 5,
    resizeMode: 'contain',
  },
  detailPart: {
    flex: 2,
    height: Dimensions.get('window').height * 0.6,
  },
  detailScrollPart: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 24,
    paddingTop: 24,
    paddingBottom: 24,
  },
  textTitle: {
    color: colors.text.primary,
    fontSize: 24,
    lineHeight: 24,
    fontFamily: fonts.telkomsel[700],
    marginTop: 16,
  },
  textArtist: {
    marginTop: 4,
    fontSize: 14,
    color: colors.text.secondary,
    fontFamily: fonts.primary[400],
  },
  scrollPadding: {
    paddingBottom: 60,
  },
  bookmarkButtonDisabled: {
    backgroundColor: colors.primary,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50 / 2,
    position: 'absolute',
    top: Dimensions.get('window').height * 0.38,
    right: Dimensions.get('window').width * 0.08,
  },
  bookmarkButton: {
    backgroundColor: colors.button.disable.background,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50 / 2,
    position: 'absolute',
    top: Dimensions.get('window').height * 0.38,
    right: Dimensions.get('window').width * 0.08,
  },
});
