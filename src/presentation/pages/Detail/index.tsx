import { View, ScrollView, Text, TouchableOpacity } from 'react-native';
import React, { useState, useEffect, useCallback } from 'react';
import {
  DetailCard,
  DetailModal,
  DetailThumbnail,
  Header,
} from '../../components';
import {
  checkFavorite,
  colors,
  getDataFavorite,
  removeDataFavorite,
  removeFromFavorite,
  showError,
  showSuccess,
  storeDataAfterRemoved,
  storeDataFavorite,
} from '../../../application';
import Icons from 'react-native-vector-icons/Ionicons';
import { styles } from './detail-styles';

const Detail = ({ navigation, route }: { navigation: any; route: any }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const itemDetail = route.params;

  const getDataFavoriteFromLocal = useCallback(async (resultFromAPI: any) => {
    const result = await getDataFavorite();
    if (result) {
      const checkResult = checkFavorite(result, resultFromAPI);
      setIsFavorite(checkResult);
      // setLoading(false);
    }
  }, []);

  useEffect(() => {
    getDataFavoriteFromLocal(itemDetail);
  }, [getDataFavoriteFromLocal, itemDetail]);

  const detailCardPressed = (detailTitle: string, detailContent: string) => {
    setTitle(detailTitle);
    setContent(detailContent);
    setModalVisible(true);
  };

  const setFavoriteItem = async () => {
    storeDataFavorite(itemDetail);
    setIsFavorite(true);
    showSuccess('Added to Favorite');
  };

  const removeFavoriteItem = async () => {
    try {
      const result = await getDataFavorite();
      if (result) {
        const afterRemoved = removeFromFavorite(result, itemDetail);
        await removeDataFavorite();
        storeDataAfterRemoved(afterRemoved);
        setIsFavorite(false);
        showSuccess('Removed from Favorite');
      }
    } catch (e) {
      showError('Error Remove Item from Favorite');
    }
  };

  return (
    <>
      <View style={styles.page}>
        <Header onPress={() => navigation.goBack()} />
        <DetailThumbnail imageId={itemDetail.image_id} />
        <View style={styles.detailPart}>
          <View style={styles.detailScrollPart}>
            <Text numberOfLines={2} style={styles.textTitle}>
              {itemDetail.title}
            </Text>
            <Text style={styles.textArtist}>
              {itemDetail.artist_title
                ? itemDetail.artist_title
                : 'Unknown Artist'}
            </Text>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={styles.scrollPadding}>
              <DetailCard
                title="Inscriptions"
                content={itemDetail.inscriptions}
                onPress={() =>
                  detailCardPressed('Inscriptions', itemDetail.inscriptions)
                }
              />
              <DetailCard
                title="Provenance Text"
                content={itemDetail.provenance_text}
                onPress={() =>
                  detailCardPressed(
                    'Provenance Text',
                    itemDetail.provenance_text,
                  )
                }
              />
              <DetailCard
                title="Publication History"
                content={itemDetail.publication_history}
                onPress={() =>
                  detailCardPressed(
                    'Publication History',
                    itemDetail.publication_history,
                  )
                }
              />
              <DetailCard
                title="Exhibition History"
                content={itemDetail.exhibition_history}
                onPress={() =>
                  detailCardPressed(
                    'Exhibition History',
                    itemDetail.exhibition_history,
                  )
                }
              />
            </ScrollView>
          </View>
        </View>
        <TouchableOpacity
          style={
            isFavorite ? styles.bookmarkButtonDisabled : styles.bookmarkButton
          }
          onPress={isFavorite ? removeFavoriteItem : setFavoriteItem}>
          <Icons
            name="heart"
            size={25}
            color={
              isFavorite ? colors.text.menuActive : colors.button.disable.text
            }
          />
        </TouchableOpacity>
      </View>
      <DetailModal
        title={title}
        content={content}
        isVisible={modalVisible}
        onRequestClose={() => setModalVisible(!modalVisible)}
        onPress={() => setModalVisible(!modalVisible)}
      />
    </>
  );
};

export default Detail;
