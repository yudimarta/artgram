import { Image, View } from 'react-native';
import React, { useCallback, useEffect } from 'react';
import { articLogo } from '../../assets/';
import { styles } from './splash-styles';
import { useDispatch } from 'react-redux';
import { getImageByPaginate } from '../../../infrastructure';
import { showError } from '../../../application';

const Splash = ({ navigation }: { navigation: any }) => {
  const dispatch = useDispatch();

  const getImageByPaginateList = useCallback(
    async (pages: number) => {
      const data = await getImageByPaginate(pages);
      if (data) {
        dispatch({
          type: 'SET_ITEMS',
          value: data,
        });
        navigation.replace('MainApp');
      } else {
        showError(
          'Sorry, there is a problem fetching the data, please try again later',
        );
      }
    },
    [dispatch, navigation],
  );

  useEffect(() => {
    getImageByPaginateList(1);
  }, [getImageByPaginateList]);

  return (
    <View style={styles.container}>
      <Image
        source={articLogo}
        resizeMode="center"
        style={styles.imageHeight}
      />
    </View>
  );
};

export default Splash;
