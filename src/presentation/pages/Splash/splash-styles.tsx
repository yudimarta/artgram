import { StyleSheet } from 'react-native';
import { colors } from '../../../application';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageHeight: {
    maxHeight: 200,
    borderRadius: 30,
  },
});
