import { View, Image } from 'react-native';
import React from 'react';
import { styles } from './detail-thumbnail-styles';

interface DetailThumbnailProps {
  imageId: string;
}

const DetailThumbnail = (props: DetailThumbnailProps) => {
  const { imageId } = props;
  return (
    <View style={styles.ImageContainer}>
      <Image
        source={{
          uri: `https://www.artic.edu/iiif/2/${imageId}/full/843,/0/default.jpg`,
        }}
        style={styles.ImageSize}
      />
    </View>
  );
};

export default DetailThumbnail;
