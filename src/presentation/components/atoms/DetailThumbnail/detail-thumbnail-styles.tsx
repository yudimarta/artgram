import { StyleSheet, Dimensions } from 'react-native';

export const styles = StyleSheet.create({
  ImageContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  ImageSize: {
    height: Dimensions.get('window').width * 0.6,
    width: Dimensions.get('window').width * 0.6,
    borderRadius: 5,
    resizeMode: 'contain',
  },
});
