import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  textMenuActive: {
    fontSize: 10,
    color: colors.text.menuActive,
    fontFamily: fonts.telkomsel[700],
  },
  textMenuInactive: {
    fontSize: 10,
    color: colors.text.menuInactive,
    fontFamily: fonts.telkomsel[700],
  },
});
