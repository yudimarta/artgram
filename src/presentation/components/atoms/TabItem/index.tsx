import { Text, TouchableOpacity } from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { colors } from '../../../../application/utilities/';
import { styles } from './tabitem-styles';

interface TabItemProps {
  title: 'Home' | 'Favorite';
  isActive: boolean;
  onPress: () => void;
  onLongPress: () => void;
}

const TabItem = (props: TabItemProps) => {
  const { title, isActive, onPress, onLongPress } = props;

  const Icon = () => {
    if (title === 'Home') {
      return isActive ? (
        <Icons name="home" size={25} color={colors.text.menuActive} />
      ) : (
        <Icons name="home" size={25} color={colors.text.menuInactive} />
      );
    } else {
      return isActive ? (
        <Icons name="heart" size={25} color={colors.text.menuActive} />
      ) : (
        <Icons name="heart" size={25} color={colors.text.menuInactive} />
      );
    }
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={isActive ? styles.textMenuActive : styles.textMenuInactive}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default TabItem;
