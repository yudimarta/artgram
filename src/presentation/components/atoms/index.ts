import TabItem from './TabItem';
import PaintingThumbnail from './PaintingThumbnail';
import DetailThumbnail from './DetailThumbnail';

export { TabItem, PaintingThumbnail, DetailThumbnail };
