import { StyleSheet, Dimensions } from 'react-native';

export const styles = StyleSheet.create({
  imageSize: {
    height: Dimensions.get('window').width * 0.27,
    width: Dimensions.get('window').width * 0.27,
    borderRadius: 10,
    marginVertical: 10,
  },
});
