import { Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { styles } from './painting-thumbnail-styles';

interface PaintingThumbnailProps {
  imageUrl: string;
  onPress: () => void;
}

const PaintingThumbnail = (props: PaintingThumbnailProps) => {
  const { imageUrl, onPress } = props;

  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        source={{
          uri: `https://www.artic.edu/iiif/2/${imageUrl}/full/843,/0/default.jpg`,
        }}
        resizeMode="cover"
        style={styles.imageSize}
      />
    </TouchableOpacity>
  );
};

export default PaintingThumbnail;
