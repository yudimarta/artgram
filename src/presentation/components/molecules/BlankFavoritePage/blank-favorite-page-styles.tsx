import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    width: 168,
    height: 243,
    resizeMode: 'center',
  },
  textTitle: {
    color: colors.text.primary,
    fontSize: 20,
    fontFamily: fonts.primary[600],
  },
  textSubtitle: {
    color: colors.text.secondary,
    fontSize: 14,
    fontFamily: fonts.primary[400],
  },
  buttonBack: {
    backgroundColor: colors.primary,
    height: 60,
    width: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 24,
  },
});
