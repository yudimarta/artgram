import { View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { emptyFolder } from '../../../assets';
import { colors } from '../../../../application';
import Icons from 'react-native-vector-icons/Ionicons';
import { styles } from './blank-favorite-page-styles';

interface BlankFavoritePageProps {
  onPress: () => void;
}

const BlankFavoritePage = (props: BlankFavoritePageProps) => {
  const { onPress } = props;
  return (
    <View style={styles.container}>
      <Image
        source={emptyFolder}
        resizeMode="center"
        style={styles.ImageStyle}
      />
      <Text style={styles.textTitle}>Oops! No Item</Text>
      <Text style={styles.textSubtitle}>
        Browse and Decide your Favorite Arts
      </Text>
      <TouchableOpacity style={styles.buttonBack} onPress={onPress}>
        <Icons name="add" size={35} color={colors.secondary} />
      </TouchableOpacity>
    </View>
  );
};

export default BlankFavoritePage;
