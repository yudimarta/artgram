import { View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native';
import React from 'react';
import { styles } from './detail-modal-styles';

interface DetailModalProps {
  title: string;
  content: string;
  isVisible: boolean;
  onRequestClose: () => void;
  onPress: () => void;
}

const DetailModal = (props: DetailModalProps) => {
  const { title, content, isVisible, onRequestClose, onPress } = props;
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={onRequestClose}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.textTitle}>{title}</Text>
          <View style={styles.scrollHeight}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Text style={styles.textContent}>{content}</Text>
            </ScrollView>
          </View>
          <TouchableOpacity style={styles.buttonClose} onPress={onPress}>
            <Text style={styles.textButton}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default DetailModal;
