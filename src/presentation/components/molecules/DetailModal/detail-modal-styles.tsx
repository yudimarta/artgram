import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: colors.primary,
    paddingHorizontal: 24,
    paddingVertical: 10,
    alignSelf: 'center',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 32,
  },
  textButton: {
    color: colors.text.menuActive,
    fontFamily: fonts.primary[700],
    textAlign: 'center',
    fontSize: 12,
  },
  textTitle: {
    fontFamily: fonts.primary[700],
    fontSize: 16,
    color: colors.text.primary,
  },
  textContent: {
    fontFamily: fonts.primary[400],
    fontSize: 14,
    color: colors.text.secondary,
    marginTop: 16,
  },
  scrollHeight: {
    height: Dimensions.get('window').height * 0.3,
    marginTop: 24,
  },
});
