import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  card: {
    backgroundColor: colors.white,
    borderBottomColor: colors.loadingBackground,
    borderBottomWidth: 0.3,
    paddingVertical: 16,
    marginTop: 1,
  },
  container: {
    flexDirection: 'row',
  },
  imageContainer: {
    marginRight: 16,
  },
  propertiesContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  imageSize: {
    height: Dimensions.get('window').width * 0.25,
    width: Dimensions.get('window').width * 0.25,
    borderRadius: 10,
  },
  textTitle: {
    fontFamily: fonts.telkomsel[700],
    fontSize: 16,
    color: colors.text.primary,
  },
  textArtist: {
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    fontSize: 12,
    marginTop: 4,
  },
  textCategory: {
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    fontSize: 14,
    textAlignVertical: 'bottom',
  },
  bottomProperties: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  deleteButton: {
    backgroundColor: colors.error,
    padding: 4,
    borderRadius: 10,
    width: 25,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
