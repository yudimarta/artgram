import { View, Text, TouchableOpacity, Image } from 'react-native';
import React from 'react';
import { colors } from '../../../../application';
import Icons from 'react-native-vector-icons/Ionicons';
import { styles } from './favorite-card-styles';

interface FavoriteCardProps {
  title: string;
  imageUrl: string;
  artist: string;
  category: string;
  onPress: () => void;
  onDelete: () => void;
}

const FavoriteCard = (props: FavoriteCardProps) => {
  const { title, imageUrl, artist, category, onPress, onDelete } = props;
  return (
    <TouchableOpacity style={styles.card} onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            source={{
              uri: `https://www.artic.edu/iiif/2/${imageUrl}/full/843,/0/default.jpg`,
            }}
            resizeMode="cover"
            style={styles.imageSize}
          />
        </View>
        <View style={styles.propertiesContainer}>
          <View>
            <Text
              style={styles.textTitle}
              numberOfLines={1}
              ellipsizeMode="tail">
              {title}
            </Text>
            <Text style={styles.textArtist}>{artist}</Text>
          </View>
          <View style={styles.bottomProperties}>
            <Text style={styles.textCategory}>{category}</Text>
            <TouchableOpacity style={styles.deleteButton} onPress={onDelete}>
              <Icons name="trash" size={15} color={colors.white} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default FavoriteCard;
