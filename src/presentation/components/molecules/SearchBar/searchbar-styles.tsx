import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
  },
  searchBarContainer: {
    height: Dimensions.get('window').height * 0.1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    justifyContent: 'center',
    paddingTop: 16,
  },
  searchBar: {
    flexDirection: 'row',
    backgroundColor: '#F3F1F1',
    borderRadius: 8,
    width: '100%',
  },
  searchBarTextInput: {
    height: 56,
    width: 320,
    fontSize: 14,
    lineHeight: 21,
    paddingLeft: 13,
    fontFamily: fonts.primary[300],
  },
  textResult: {
    fontSize: 14,
    paddingHorizontal: 24,
    marginTop: 12,
    fontFamily: fonts.primary[300],
    marginBottom: 12,
  },
  closeIconMargin: {
    marginLeft: 13,
    marginTop: 17,
  },
  searchIconMargin: {
    marginLeft: 13,
    marginTop: 17,
  },
});
