import { View, TouchableOpacity, TextInput } from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { styles } from './searchbar-styles';

interface SearchBarProps {
  searchedKeyWord: string;
  placeHolder: string;
  onPress: () => void;
  onChangeText?: (val: string) => void;
  onSubmitEditing: () => void;
}

const SearchBar = (props: SearchBarProps) => {
  const {
    searchedKeyWord,
    placeHolder,
    onPress,
    onChangeText,
    onSubmitEditing,
  } = props;
  return (
    <View style={styles.searchBarContainer}>
      <View style={styles.searchBar}>
        {searchedKeyWord ? (
          <TouchableOpacity onPress={onPress}>
            <Icons
              name="close-outline"
              size={25}
              color="#979797"
              style={styles.closeIconMargin}
            />
          </TouchableOpacity>
        ) : (
          <Icons
            name="search"
            size={20}
            color="#979797"
            style={styles.closeIconMargin}
          />
        )}
        <TextInput
          placeholder={placeHolder}
          placeholderTextColor="#8E8E93"
          style={styles.searchBarTextInput}
          value={searchedKeyWord}
          autoFocus={false}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
        />
      </View>
    </View>
  );
};

export default SearchBar;
