import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    width: 168,
    height: 243,
    resizeMode: 'center',
  },
  textTitle: {
    color: colors.text.primary,
    fontSize: 20,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
  },
  textSubtitle: {
    color: colors.text.secondary,
    fontSize: 14,
    fontFamily: fonts.primary[400],
  },
  refreshButton: {
    backgroundColor: colors.primary,
    height: 60,
    width: 150,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  textButton: {
    fontFamily: fonts.primary[600],
    fontSize: 14,
    color: colors.white,
  },
});
