import { View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { Folder } from '../../../assets';
import { styles } from './blank-home-page-styles';

interface BlankHomePageProps {
  onPress: () => void;
}

const BlankHomePage = (props: BlankHomePageProps) => {
  const { onPress } = props;
  return (
    <View style={styles.container}>
      <Image source={Folder} resizeMode="center" style={styles.ImageStyle} />
      <Text style={styles.textTitle}>
        Oops! We can't show you our collection right now
      </Text>
      <Text style={styles.textSubtitle}>Please try again in a few minutes</Text>
      <TouchableOpacity style={styles.refreshButton} onPress={onPress}>
        <Text style={styles.textButton}>Refresh</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BlankHomePage;
