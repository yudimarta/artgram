import { View, Text, TouchableOpacity } from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { styles } from './header-styles';
import { colors } from '../../../../application';

interface HeaderProps {
  onPress: () => void;
}

const Header = (props: HeaderProps) => {
  const { onPress } = props;
  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text numberOfLines={1} style={styles.textTitle}>
          Detail
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={onPress}>
          <Icons name="chevron-back" size={25} color={colors.text.primary} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Header;
