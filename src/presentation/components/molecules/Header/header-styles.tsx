import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 50,
    paddingVertical: 10,
  },
  buttonContainer: {
    position: 'absolute',
    left: 20,
  },
});
