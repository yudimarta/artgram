import { StyleSheet, Dimensions } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: colors.primary,
    paddingHorizontal: 24,
    paddingVertical: 10,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButtonClose: {
    color: colors.text.menuActive,
    fontFamily: fonts.primary[700],
    textAlign: 'center',
    fontSize: 12,
  },
  textContent: {
    fontFamily: fonts.primary[400],
    fontSize: 14,
    color: colors.text.secondary,
    marginTop: 16,
  },
  buttonContainer: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    width: Dimensions.get('window').width * 0.6,
    marginTop: 32,
  },
  buttonDelete: {
    backgroundColor: colors.white,
    paddingHorizontal: 24,
    paddingVertical: 10,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: colors.error,
    borderWidth: 0.5,
  },
  textButtonDelete: {
    color: colors.error,
    fontFamily: fonts.primary[700],
    textAlign: 'center',
    fontSize: 12,
  },
});
