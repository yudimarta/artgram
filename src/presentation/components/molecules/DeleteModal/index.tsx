import { View, Text, Modal, TouchableOpacity } from 'react-native';
import React from 'react';
import { styles } from './delete-modal-styles';

interface DeleteModalProps {
  isVisible: boolean;
  onRequestClose: () => void;
  onPress: () => void;
  onDelete: () => void;
}

const DeleteModal = (props: DeleteModalProps) => {
  const { isVisible, onRequestClose, onPress, onDelete } = props;
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={onRequestClose}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.textContent}>
            Remove Item from your favorite?
          </Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttonClose} onPress={onDelete}>
              <Text style={styles.textButtonClose}>Remove</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonDelete} onPress={onPress}>
              <Text style={styles.textButtonDelete}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default DeleteModal;
