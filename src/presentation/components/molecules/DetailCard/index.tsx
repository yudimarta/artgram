import { View, Text, TouchableOpacity } from 'react-native';
import React, { useCallback, useState } from 'react';
import { styles } from './detail-card-styles';
import { NUM_OF_LINES } from '../../../../application';

interface DetailCardProps {
  title: string;
  content: string;
  onPress: () => void;
}

const DetailCard = (props: DetailCardProps) => {
  const { title, content, onPress } = props;
  const [showMore, setShowMore] = useState(false);
  const onTextLayout = useCallback(text => {
    setShowMore(text.nativeEvent.lines.length > NUM_OF_LINES);
  }, []);
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.card_button}
        disabled={content === null}
        onPress={showMore ? onPress : () => {}}>
        <Text style={styles.textTitle}>{title}</Text>
        <Text
          style={styles.textContent}
          numberOfLines={NUM_OF_LINES}
          onTextLayout={onTextLayout}>
          {content ? content : 'Unavailable'}
        </Text>
        {showMore ? (
          <View style={styles.linkMargin}>
            <Text style={styles.link}>See More</Text>
          </View>
        ) : null}
      </TouchableOpacity>
    </View>
  );
};

export default DetailCard;
