import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginTop: 16,
  },
  card_button: {
    borderRadius: 16,
    paddingVertical: 12,
    justifyContent: 'center',
  },
  textTitle: {
    fontFamily: fonts.primary[700],
    fontSize: 14,
    color: colors.text.primary,
  },
  textContent: {
    fontFamily: fonts.primary[400],
    fontSize: 12,
    color: colors.text.secondary,
    marginTop: 4,
  },
  link: {
    fontFamily: fonts.primary[600],
    fontSize: 12,
    color: colors.text.link,
    textAlign: 'right',
  },
  linkMargin: {
    marginTop: 8,
  },
});
