import { StyleSheet } from 'react-native';
import { colors } from '../../../../application';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 53,
    paddingVertical: 12,
    backgroundColor: colors.primary,
  },
});
