import { View } from 'react-native';
import React from 'react';
import { TabItem } from '../../atoms';
import { styles } from './bottom-tab-navigator-styles';

interface BottomTabNavigatorProps {
  state: any;
  descriptors: any;
  navigation: any;
}

const BottomTabNavigator = (props: BottomTabNavigatorProps) => {
  const { state, descriptors, navigation } = props;
  return (
    <View style={styles.container}>
      {state.routes.map(
        (
          route: { key: string | number; name: any },
          index: React.Key | null | undefined,
        ) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TabItem
              key={index}
              title={label}
              isActive={isFocused}
              onPress={onPress}
              onLongPress={onLongPress}
            />
          );
        },
      )}
    </View>
  );
};

export default BottomTabNavigator;
