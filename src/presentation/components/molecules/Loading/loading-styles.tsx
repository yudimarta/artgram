import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../application';

export const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.loadingBackground,
    width: '100%',
    height: '100%',
  },
  text: {
    fontSize: 18,
    fontFamily: fonts.primary[600],
    color: colors.secondary,
  },
});
