import { View, Text, ActivityIndicator } from 'react-native';
import React from 'react';
import { colors } from '../../../../application';
import { styles } from './loading-styles';

const Loading = () => {
  return (
    <View style={styles.wrapper}>
      <ActivityIndicator size="large" color={colors.secondary} />
      <Text style={styles.text}>Loading...</Text>
    </View>
  );
};

export default Loading;
