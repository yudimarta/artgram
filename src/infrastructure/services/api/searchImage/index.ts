import axios from 'axios';
import Config from 'react-native-config';
import { ITEMS, LIMIT_PER_PAGE } from '../../../../application';

export const searchImage = async (page: number, keyword: string) => {
  try {
    const response = await axios.get(
      Config.BASE_URL +
        '/search?page=' +
        page +
        '&limit=' +
        LIMIT_PER_PAGE +
        '&q=' +
        keyword +
        '&fields=' +
        ITEMS,
    );
    if (response) {
      if (response.status === 200) {
        return response.data.data;
      } else {
        return null;
      }
    }
  } catch (error) {}
};
