import axios from 'axios';
import Config from 'react-native-config';
import { ITEMS, LIMIT_PER_PAGE } from '../../../../application';

export const getImageByPaginate = async (page: number) => {
  try {
    const response = await axios.get(
      Config.BASE_URL +
        '?page=' +
        page +
        '&limit=' +
        LIMIT_PER_PAGE +
        '&fields=' +
        ITEMS,
    );
    if (response) {
      if (response.status === 200) {
        return response.data.data;
      } else {
        return null;
      }
    }
  } catch (e: any) {}
};
