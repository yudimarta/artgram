import { getImageByPaginate } from './getImageByPaginate';
import { searchImage } from './searchImage';

export { getImageByPaginate, searchImage };
