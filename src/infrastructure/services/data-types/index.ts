export interface ImagePagination {
  id: number;
  api_link: string;
  title: string;
  thumbnail: {
    lqip: string;
    width: number;
    height: number;
    alt_text: string;
  };
  artist_display: string;
  place_of_origin: string;
  inscription: string;
  credit_line: string;
  publication_history: string;
  exhibition_history: string;
  provenance_text: string;
  artist_title: string;
  artist_id: number;
  image_id: string;
  timestamp: string;
}
